//
//  RestaurantCell.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 12/19/17.
//  Copyright © 2017 Muhammad Asif. All rights reserved.
//

import UIKit

class RestaurantCell: UITableViewCell {

    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var restaurantStateView: UIView!
}

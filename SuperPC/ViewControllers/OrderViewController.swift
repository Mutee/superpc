//
//  OrderViewController.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 1/8/18.
//  Copyright © 2018 Muhammad Asif. All rights reserved.
//

import UIKit
import SVProgressHUD

class OrderViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ProductCellDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    

    //MARK: - IBOUTLETS
    @IBOutlet weak var productsTableView: UITableView!
    @IBOutlet weak var pickersContainerView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!

    //MARK: - PROPERTIES
    var cartProducts = [CartProduct(product: .thermalPaperForReceiptPaper, quantity: 0), CartProduct(product: .onePlyPapaerForKitchenPrinter, quantity: 0), CartProduct(product: .twoPlyPapaerForKitchenPrinter, quantity: 0), CartProduct(product: .threePlyPapaerForKitchenPrinter, quantity: 0), CartProduct(product: .twelwePackOfInkCartridgesSPS, quantity: 0)]
        
    var restaurant: Restaurant!
   
    let inkCartidgesArray = [Product.twelwePackOfInkCartridgesSPS, Product.twelwePackOfInkCartridgesSNBC, Product.twelwePackOfInkCartridgesEpson, Product.twelwePackOfInkCartridgesStar, Product.twelwePackOfInkCartridgesOther]
    
    var selectedInkCartidge: Product!
    
    //MARK: - VIEW CONTROLLER'S LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
//        selectedInkCartidge = inkCartidgesArray[0]
        pickersContainerView.removeFromSuperview()
    }
    
    //MARK: - TEXTFIELD DELEGATES
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
  
    //MARK: - SERVICE CALLS
    func placeOrder(printerBrandName: String = "") {
        
        let cartProductsWithSomeQuantity = cartProducts.filter{$0.quantity > 0}
        if (cartProductsWithSomeQuantity.count) == 0 {
            return
        }
        
        SVProgressHUD.show(withStatus: "Placing Order")
       
        weak var weakSelf = self
       
        EmailService.sendEmail(emailType: "order", senderEmail: UserService.persistedUser()!.email, subject: "Supplies Order - \(restaurant.name)", restaurantId: restaurant.streamingId, cartProductsForOrder: cartProductsWithSomeQuantity, kitchenPrinterBrand: selectedInkCartidge?.printerBrand ?? "") { (success, message, error) in
           
            SVProgressHUD.dismiss()
           
            if weakSelf != nil {
                if error != nil {
                    weakSelf!.presentOkAlert(error!.localizedErrorTitle(), message: error!.localizedErrorDescription())
                    return
                }
                
                if success! == true {
                    SVProgressHUD.showSuccess(withStatus: message!)
                    weakSelf!.navigationController?.popViewController(animated: true)
                    return
                }
                weakSelf!.presentOkAlert(AppStringConstants.BrandName, message: message!) {
                }
            }
        }
    }
  
    //MARK: TABLEVIEW DELGATES AND DATASOURCES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartProducts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cartProduct = cartProducts[indexPath.row]

        if cartProduct.product.isInkCartidge {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.InkCartridgeCell.rawValue) as! InkCartridgeCell
            cell.nameTextField.text = ""
            if selectedInkCartidge != nil {
                cell.nameLabel.text = selectedInkCartidge.rawValue
                cartProducts[indexPath.row].product = selectedInkCartidge
            }
            else {
                cell.nameLabel.text = cartProduct.product.rawValueForServer
            }
            cell.productQuantityTextfield.text = "\(cartProduct.quantity)"
            cell.nameTextField.inputView = pickersContainerView
            cell.tag = indexPath.row
            cell.delegate = self
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.ProductCell.rawValue) as! ProductCell
        cell.productNameLabel.text = cartProduct.product.rawValue
        cell.productQuantityTextfield.text = "\(cartProduct.quantity)"
        cell.tag = indexPath.row
        cell.delegate = self
        
        return cell

    }

    //MARK: Product CELL DELEGATES
    func cellPlusButtonTouchedUpInside(_ tag: Int) {
        cartProducts[tag].quantity += 1
        productsTableView.reloadData()
    }
    func cellMinusButtonTouchedUpInside(_ tag: Int) {
        if cartProducts[tag].quantity > 0 {
            cartProducts[tag].quantity -= 1
            productsTableView.reloadData()
        }
    }
    func cellQuantityTextFieldDidChange(_ tag: Int, textFieldText: String) {
        if textFieldText.intValue >= 0 {
            cartProducts[tag].quantity = textFieldText.intValue
            productsTableView.reloadData()
        }
    }
    func cellQuantityTextFieldEditingCancelled(_ tag: Int) {
        productsTableView.reloadData()
    }

    //MARK: PickerView Delegates And Data Sources
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return inkCartidgesArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return inkCartidgesArray[row].printerBrand
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedInkCartidge = inkCartidgesArray[row]
    }

    @IBAction func doneOnPickerContainerPressed(_ sender: UIBarButtonItem) {
        selectedInkCartidge = inkCartidgesArray[pickerView.selectedRow(inComponent: 0)]
        productsTableView.reloadData()
    }
    //MARK: - IBACTIONS
    @IBAction func backButtonPressed(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func placeOrderButtonPressed(_ sender: UIButton) {
        if cartProducts[cartProducts.count - 1].quantity > 0 && selectedInkCartidge == nil {
            presentOkAlert(AppStringConstants.BrandName, message: "Please select an Ink Cartridge brand.")
            return
        }
        placeOrder()
    }
}

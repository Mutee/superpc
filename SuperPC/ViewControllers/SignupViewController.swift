//
//  SignupViewController.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 1/11/18.
//  Copyright © 2018 Muhammad Asif. All rights reserved.
//

import UIKit
import SVProgressHUD

class SignupViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, RestaurantInputCellDelegate {

    //MARK: - IBOUTLETS
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var restaurantsTableView: UITableView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!

    @IBOutlet weak var restaurantsTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollVieeContentHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var backButton: UIButton!
    
    //MARK: - PROPERTIES
    var restaurantNames: [String] = [""]
    var selectedTextField: UITextField!
    var lastOffset: CGPoint!
    var keyboardHeight: CGFloat!
    var indexOfActiveFieldInRestaurantsTableView: Int = -1
    var scrollContentHeight: CGFloat = 0.0
    
    //MARK: - VIEW CONTROLLER LIFE CYCLE
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollContentHeight = scrollVieeContentHeightConstraint.constant
        addMeAsObserverToNotifications()
        reloadTableView()
    }

    func reloadTableView() {
        restaurantsTableView.reloadData()
        restaurantsTableViewHeightConstraint.constant = CGFloat((restaurantNames.count * 68) + 45)
        scrollVieeContentHeightConstraint.constant = scrollContentHeight - (68 + 45) + restaurantsTableViewHeightConstraint.constant
        
//        scrollContentHeight = scrollVieeContentHeightConstraint.constant

    }
    //MARK: - SERVICE CALLS
    func signup() {
        SVProgressHUD.show(withStatus: "Signing Up")
        weak var weakSelf = self
        EmailService.sendEmail(emailType: "sign_up", emailAddressOfNewUser: emailTextField.text!, nameOfNewUser: nameTextField.text!, newRestaurants: restaurantNames.filter{$0 != ""}, password: passwordTextField.text!) { (success, message, error) in
            
            SVProgressHUD.dismiss()
            
            if weakSelf != nil {
                if error != nil {
                    if !weakSelf!.shouldHandleError(error: error!) {
                        return
                    }
                    weakSelf!.presentOkAlert(error!.localizedErrorTitle(), message: error!.localizedErrorDescription())
                    return
                }
                if success! == true {
                    SVProgressHUD.showSuccess(withStatus: message!)
                    weakSelf?.navigationController?.popViewController(animated: true)
                    return
                }

                weakSelf!.presentOkAlert(AppStringConstants.BrandName, message: message!) {
//                    if success! == true {
//                        weakSelf?.navigationController?.popViewController(animated: true)
//                    }
                }
            }
        }
    }
  
    //MARK: TEXTFIELD DELEGATES
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        indexOfActiveFieldInRestaurantsTableView = -1
        lastOffset = self.scrollView.contentOffset
        selectedTextField = textField
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        selectedTextField = nil
        if textField == nameTextField {
            let cell = restaurantsTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! RestaurantInputCell
            cell.restaurantNameTextField.becomeFirstResponder()
        }
        else if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        }
        else if textField == passwordTextField {
            confirmPasswordTextField.becomeFirstResponder()
        }
        else if textField == confirmPasswordTextField {
//            confirmPasswordTextField.resignFirstResponder()
        }
        return true
    }
    
    //MARK: KEYBOARD LISTENERS
    @objc func keyboardDidShow(_ notification: Notification) {
        
        if let _ = ((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.keyboardHeight = (((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size.height)!
            
            self.scrollVieeContentHeightConstraint.constant = scrollVieeContentHeightConstraint.constant + self.keyboardHeight
            
            var distanceToBottom: CGFloat = 0.0
            if selectedTextField != nil {
                distanceToBottom = self.scrollView.frame.size.height - (selectedTextField?.frame.origin.y)! - (selectedTextField?.frame.size.height)!
            }
            else {
                distanceToBottom = self.scrollView.frame.size.height - (restaurantsTableView.frame.origin.y) -  CGFloat((indexOfActiveFieldInRestaurantsTableView + 1) * 68)
            }
            let collapseSpace = keyboardHeight - distanceToBottom + 80
            if collapseSpace < 0 {
                // no collapse
                return
            }
            // set new offset for scroll view
            UIView.animate(withDuration: 0.3, animations: {
                // scroll to the position above keyboard 10 points
                self.scrollView.contentOffset = CGPoint(x: self.lastOffset.x, y: collapseSpace + 10)
            })
        }
        
    }
    @objc func keyboardWillHide(_ notification: Notification) {
        
        if keyboardHeight != nil {
            self.scrollVieeContentHeightConstraint.constant -= keyboardHeight
            keyboardHeight = nil
        }
    }

    //MARK: - TABLEVIEW DELEGATES AND DATA SOURCES
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurantNames.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < restaurantNames.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.RestaurantInputCell.rawValue) as! RestaurantInputCell
            cell.restaurantNameTextField.text = restaurantNames[indexPath.row]
            cell.tag = indexPath.row
            cell.delegate = self
            return cell
        }
       
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.AddRestauarntCell.rawValue) as! AddRestauarntCell
        return cell

        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row >= restaurantNames.count {
            restaurantNames.append("")
            reloadTableView()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < restaurantNames.count {
            return 68
        }
        return 45
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if restaurantNames.count == 1 || indexPath.row == restaurantNames.count {
            return false
        }
        return true
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.delete
    }
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Delete"
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            restaurantNames.remove(at: indexPath.row)
            reloadTableView()
        }
    }

    //MARK: - RESTAURANT INPUT CELL DELEGATES
    func cellRestaurantNameTextFieldDidChange(_ tag: Int, textFieldText: String) {
        restaurantNames[tag] = textFieldText
    }
    func cellRestaurantNameTextFieldDidBeginEditing(_ tag: Int) {
        indexOfActiveFieldInRestaurantsTableView = tag
        lastOffset = self.scrollView.contentOffset
        selectedTextField = nil
    }
    func cellRestaurantNameTextFieldDidReturn(_ tag: Int) {
        indexOfActiveFieldInRestaurantsTableView = -1
        selectedTextField = nil
        
        if tag == restaurantNames.count - 1 {
            emailTextField.becomeFirstResponder()
        }
        else {
            let cell = restaurantsTableView.cellForRow(at: IndexPath(row: tag + 1, section: 0)) as! RestaurantInputCell
            cell.restaurantNameTextField.becomeFirstResponder()
        }
    }
    //MARK: NOTIFICATION CENTRE
    func addMeAsObserverToNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(SignupViewController.keyboardDidShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(SignupViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
        
    }

    //MARK: IBACTIONS
    @IBAction func signupButtonPressed(_ sender: UIButton) {
        
        //Validate for empty fields
        if nameTextField.text == ""  || emailTextField.text == "" || restaurantNames[0] == "" || passwordTextField.text == "" || confirmPasswordTextField.text == "" {
           
            presentOkAlert(AppStringConstants.BrandName, message: AppStringConstants.EmptyFieldsMessage)
            return
        }
        
        //Check for valid email address
        if !emailTextField.text!.isValidEmail() {
            
            presentOkAlert(AppStringConstants.BrandName, message: AppStringConstants.InvalidEmailMessage)
            return
        }
        
        //Match password and confirm password
        if passwordTextField.text! != confirmPasswordTextField.text! {
           
            presentOkAlert(AppStringConstants.BrandName, message: AppStringConstants.PasswordsDontMatch)
            return
        }
        
        //Password must be atleast 7 characters long and has atleast 1 number and 1 letter.
        if !passwordTextField.text!.containsLetters() || !passwordTextField.text!.containsNumericCharacters() {
            presentOkAlert(AppStringConstants.BrandName, message: AppStringConstants.InvalidPassword)
            return

        }
        //Valid input. Send to server
        signup()

    }
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
}

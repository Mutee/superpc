//
//  SalesReportViewController.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 12/20/17.
//  Copyright © 2017 Muhammad Asif. All rights reserved.
//

import UIKit
import Spring
import SVProgressHUD

let DateFormat = "M/dd/YYYY"

class SalesReportViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

    //MARK: IBOUTLETS
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var datePickerContainerView: UIView!
    @IBOutlet weak var fromDateTextField: UITextField!
    @IBOutlet weak var toDateTextField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var totalSalesLabel: UILabel!
    @IBOutlet weak var reportsTableView: UITableView!
    @IBOutlet weak var placeholderLabel: UILabel!
    
    //MARK: - PROPERTIES
    var selectedTextField: UITextField!
    var restaurant: Restaurant!
    var report: Report!
    var voidReports: [VoidReport] = []
    var discounts: [Discount] = []
    
    //MARK: - VIEW CONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        placeholderLabel.text = report.placeholder
        titleLabel.text = report.rawValue
       
        if report == .sales {
            totalSalesLabel.isHidden = false
            reportsTableView.isHidden = true
        }
        else if report == .void || report == .discount {
            totalSalesLabel.isHidden = true
            reportsTableView.isHidden = false
        }
        
        datePickerContainerView.removeFromSuperview()
        fromDateTextField.inputView = datePickerContainerView
        toDateTextField.inputView = datePickerContainerView
        
        fromDateTextField.text = stringFromDate(date: Date() as NSDate, format: DateFormat)
        toDateTextField.text = stringFromDate(date: Date() as NSDate, format: DateFormat)
        
        getReport()
    }
    
    func setSalesReportLabels(totalSales: Double?) {
        if totalSales == nil {
            totalSalesLabel.text = "Sales: N/A"
        }
        else {
            totalSalesLabel.text = "Sales: $\(totalSales!.stringWithThousandSeperator())"
        }
    }
   
    //MARK: - SERVICE CALLS
    func getReport() {
        weak var weakSelf = self
        SVProgressHUD.show(withStatus: "")
        ReportService.getReport(reportType: report, businessPhone: restaurant.businessPhone, startDateString: fromDateTextField.text!, endDateString: toDateTextField.text!) { (totalSales, voids, discounts,  error) in
            SVProgressHUD.dismiss()
            if weakSelf != nil {
                if error != nil {
                    return
                }
                weakSelf!.setSalesReportLabels(totalSales: totalSales)
                
                if voids != nil {
                    weakSelf!.voidReports = voids!
                        weakSelf!.placeholderLabel.isHidden = voids!.count != 0
                }
                if discounts != nil {
                    weakSelf!.discounts = discounts!
                    weakSelf!.placeholderLabel.isHidden = discounts!.count != 0

                }
                
                weakSelf!.reportsTableView.reloadData()
            }
        }
    }
   
    //MARK: TABLEVIEW DELGATES AND DATASOURCES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if report == .void {
            return voidReports.count
        }
        return discounts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if report == .void {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.VoidReportCell.rawValue) as! VoidReportCell
            
            let voidReport = voidReports[indexPath.row]
            cell.voidAmountLabel.text = "Void Amount: $\(voidReport.amount.roundToPlaces(2))"
            cell.employeeLabel.text = "Employee: \(voidReport.employeeName)"
            cell.orderLabel.text = "Order: \(voidReport.orderNo)"
            cell.voidReasonLabel.text = "Void Reason: \(voidReport.reason)"
            cell.dateLabel.text = "Date/Time: \(voidReport.dateTime)"
            
            return cell

        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.DiscountReportCell.rawValue) as! DiscountReportCell
            
            let discount = discounts[indexPath.row]
            cell.discountAmountLabel.text = "Discount Amount: $\(discount.amount.roundToPlaces(2))"
            cell.discountNameLabel.text = "Discount Name: \(discount.name)"
            cell.employeeNameLabel.text = "Employee: \(discount.employeeName)"
            cell.orderNoLabel.text = "Order: \(discount.orderNo)"
            cell.dateTimeLabel.text = "Date/Time: \(discount.dateTime)"

            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 172
    }
    //MARK: - TEXTFIELD DELEGATES
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
    }
    //MARK: - IBACTIONS
    @IBAction func backButtonPressed(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func doneButtonOnDatePickerToolbarPressed(_ sender: UIBarButtonItem) {
        selectedTextField.resignFirstResponder()
        selectedTextField.text = stringFromDate(date: datePicker.date as NSDate, format: DateFormat)
        getReport()
    }
}

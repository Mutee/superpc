//
//  ProductCell.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 1/8/18.
//  Copyright © 2018 Muhammad Asif. All rights reserved.
//

import UIKit

@objc protocol ProductCellDelegate: class {
    func cellPlusButtonTouchedUpInside(_ tag: Int)
    func cellQuantityTextFieldDidChange(_ tag: Int, textFieldText: String)
    func cellQuantityTextFieldEditingCancelled(_ tag: Int)
    func cellMinusButtonTouchedUpInside(_ tag: Int)
    
}

class ProductCell: UITableViewCell, UITextFieldDelegate {
   
    //MARK: IBOUTLETS
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productQuantityTextfield: UITextField!

    //MARK: PROPERTIES
    weak var delegate:ProductCellDelegate?

    //MARK: CELL LIFE CYCLE
    override func awakeFromNib() {
        productQuantityTextfield.delegate = self
    }

    //MARK: TEXT FIELD DELEGATES
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        // Add tool bar to keyboard
        textField.addToolBarWithButtons(AppStringConstants.KeyBoardCancelButtonText, rightButtonTitle:
            AppStringConstants.KeyBoardAddButtonText, leftButtonAction: #selector(ProductCell.cancelButtonClickedOnKeyboard(_:)), rightButtonAction: #selector(ProductCell.addButtonClickedOnKeyBoard(_:)), toolBarBackgroundColor: UIColor.clear, toolBarTintColor: UIColor.black, target: self)
    }

    // MARK: TEXT FIELD TOOLBAR BUTTONS LISTENERS
    @objc func addButtonClickedOnKeyBoard(_ sender: UIBarButtonItem)
    {
        productQuantityTextfield.resignFirstResponder()
        delegate?.cellQuantityTextFieldDidChange(self.tag, textFieldText: productQuantityTextfield.text!)
    }
    @objc func cancelButtonClickedOnKeyboard(_ sender: UIBarButtonItem)
    {
        productQuantityTextfield.resignFirstResponder()
        delegate?.cellQuantityTextFieldEditingCancelled(self.tag)
    }
    
    //MARK: IBACTIONS
    @IBAction func plusButtonClicked(_ sender: UIButton) {
        delegate?.cellPlusButtonTouchedUpInside(self.tag)
    }
    @IBAction func minusButtonClicked(_ sender: UIButton) {
        delegate?.cellMinusButtonTouchedUpInside(self.tag)
    }

    
    

}

//
//  RestaurantsViewController.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 12/19/17.
//  Copyright © 2017 Muhammad Asif. All rights reserved.
//

import UIKit
import SVProgressHUD

class RestaurantsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - IBOUTLETS
    @IBOutlet weak var restaurantsTableView: UITableView!
    @IBOutlet weak var selectedRestuarantView: UIView!
    @IBOutlet weak var selectedRestaurantTitleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var callToSupportButton: UIButton!

    @IBOutlet weak var restaurantsTopView: UIView!
    @IBOutlet weak var selectedRestauranMenuTableView: UITableView!
    
    //MARK: PROPERTIES
    var restaurants: [Restaurant] = []
    var statuses: [Bool] = []
    var selectedRestaurant: Restaurant!
    var menu: [Menu]! = [.reports, .order, .submitMenuChanges, .submitSupportIssue, .videoStreaming]
    var selectedMenuIndex = -1
    
    //MARK: - VIEW CONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        restaurants = UserService.persistedUser()!.restaurants
    }
    override func viewDidAppear(_ animated: Bool) {
        refresh()
    }
    func refresh() {
        
        restaurants = UserService.persistedUser()!.restaurants
        restaurantsTableView.reloadData()
        
        if restaurants.count == 1 {
            selectedRestuarantView.isHidden = false
            restaurantsTableView.isHidden = true
            restaurantsTopView.isHidden = true
            selectedRestaurant = restaurants[0]
            backButton.isHidden = true
            setSelectedRestaurantValues()
        }
        else {
            if (selectedRestaurant != nil) && (restaurants.filter{$0.streamingId == selectedRestaurant.streamingId}.count > 0) {
                selectedRestuarantView.isHidden = false
                restaurantsTableView.isHidden = true
                restaurantsTopView.isHidden = true
                backButton.isHidden = false
                setSelectedRestaurantValues()
            }
            else {
                selectedRestuarantView.isHidden = true
                restaurantsTableView.isHidden = false
                restaurantsTopView.isHidden = false
            }
        }
    }
    func setSelectedRestaurantValues() {
        
        if UserService.persistedUser()!.haveAccessToReports == true {
            menu = [.reports, .order, .submitMenuChanges, .submitSupportIssue, .videoStreaming]
        }
        else {
            menu = [.order, .submitMenuChanges, .submitSupportIssue, .videoStreaming]
        }
        selectedRestauranMenuTableView.reloadData()
        selectedRestaurantTitleLabel.text = selectedRestaurant.name
    }
   
    //MARK: - SERVICE CALLS
    func updateData() {
        
        restaurantsTopView.isHidden = true
        restaurantsTableView.isHidden = true
        selectedRestuarantView.isHidden = true

        SVProgressHUD.show(withStatus: "Loading")
        weak var weakSelf = self
        UserService.signIn(UserService.persistedUser()!.email, password: UserService.persistedUser()!.password) { (user, error) in
            SVProgressHUD.dismiss()
            if weakSelf != nil {
                weakSelf!.refresh()
            }
        }
    }
  
    //MARK: TABLEVIEW DELGATES AND DATASOURCES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == selectedRestauranMenuTableView {
            return menu.count
        }
        return restaurants.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == selectedRestauranMenuTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.MenuCell.rawValue) as! MenuCell
            let menuItem = menu[indexPath.row]
            cell.menuTitleLabel.text = menuItem.rawValue
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.RestaurantCell.rawValue) as! RestaurantCell
        let restaurant = restaurants[indexPath.row]
        cell.restaurantNameLabel.text = restaurant.name
        if restaurant.isActive {
            cell.restaurantStateView.backgroundColor = UIColor.green
        }
        else {
            cell.restaurantStateView.backgroundColor = UIColor.red
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == selectedRestauranMenuTableView {
            selectedMenuIndex = indexPath.row
            if menu[indexPath.row] == .reports {
                if selectedRestaurant.isActive == false {
                    presentOkAlert(AppStringConstants.BrandName, message: "Please make sure your main POS server is online and connected to internet")
                    return
                }

                performSegue(withIdentifier: SegueIdentifiers.RestaurantsVCToReportsVCSegue.rawValue, sender: self)
                return
            }
            if menu[indexPath.row] == .videoStreaming {
                performSegue(withIdentifier: SegueIdentifiers.RestaurantsVCToLiveStreamVCSegue.rawValue, sender: self)
                return
            }
            if menu[indexPath.row] == .submitMenuChanges || menu[indexPath.row] == .submitSupportIssue {
                performSegue(withIdentifier: SegueIdentifiers.RestaurantsVCToEmailVCSegue.rawValue, sender: self)
                return
            }
            if menu[indexPath.row] == .order {
                performSegue(withIdentifier: SegueIdentifiers.RestaurantsVCToOrderVCSegue.rawValue, sender: self)
                return
            }
            return
        }
        selectedRestaurant = restaurants[indexPath.row]
        restaurantsTableView.isHidden = true
        restaurantsTopView.isHidden = true
        selectedRestuarantView.isHidden = false
        backButton.isHidden = false
        refresh()
    }

    @IBAction func backButtonPressed(_ sender: UIButton) {
        selectedRestaurant = nil
        updateData()
    }
    @IBAction func callToSupportButtonPressed(_ sender: UIButton) {
        let url = URL(string: "tel://7183330999")!
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func logoutButtonPressed(_ sender: UIButton) {
        
        weak var weakSelf = self
        let logoutAction = UIAlertAction(title: "Logout", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            UserService.logout()
            _ = weakSelf?.navigationController?.popToRootViewController(animated: true)

        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })

        presentAlert("SuperPC", message: "", actions: logoutAction, cancelAction, preferredStyle: .actionSheet)
//        presentAlert("SuperPc", message: "", actions: [logoutAction, cancelAction], preferredStyle: .actionSheet)
    }
    
    //MARK: - NAVIGATIONS
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifiers.RestaurantsVCToReportsVCSegue.rawValue {
            let vc = segue.destination as! ReportsViewController
            vc.restaurant = selectedRestaurant
        }
        if segue.identifier == SegueIdentifiers.RestaurantsVCToLiveStreamVCSegue.rawValue {
            let vc = segue.destination as! LiveStreamViewController
            vc.restaurant = selectedRestaurant
        }
        if segue.identifier == SegueIdentifiers.RestaurantsVCToEmailVCSegue.rawValue {
            let vc = segue.destination as! EmailViewController
            vc.restaurant = selectedRestaurant
            vc.menuType = menu[selectedMenuIndex]
        }
        if segue.identifier == SegueIdentifiers.RestaurantsVCToOrderVCSegue.rawValue {
            let vc = segue.destination as! OrderViewController
            vc.restaurant = selectedRestaurant
        }

    }
    
}

//
//  LiveStreamViewController.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 12/27/17.
//  Copyright © 2017 Muhammad Asif. All rights reserved.
//

import UIKit
import LFLiveKit

class LiveStreamViewController: UIViewController, LFLiveSessionDelegate {

    //MARK: IBOUTLETS
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var flashButton: UIButton!
    
    //MARK: PROPERTIES
    var restaurant: Restaurant!
    let stream = LFLiveStreamInfo()

    let audioConfiguration = LFLiveAudioConfiguration.default()
    let videoConfiguration = LFLiveVideoConfiguration.defaultConfiguration(for: LFLiveVideoQuality.default)!
    var session1: LFLiveSession!
    var isLive = true
    
    //MARK: - VIEW CONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        session1 = LFLiveSession(audioConfiguration: audioConfiguration, videoConfiguration: videoConfiguration)
        session1.delegate = self
        session1.preView = previewView
        session1.running = true
        session1?.captureDevicePosition = AVCaptureDevice.Position.back
        session1.muted = true

        startLive()

    }
    
    //MARK: - Event
    func startLive() -> Void {
        stream.url = "rtmp://pcsuperapp.com:1935/srs/\(restaurant.streamingId)";
        stream.streamId = restaurant.streamingId
        session1.startLive(stream)
    }
    
    func stopLive() -> Void {
        session1.stopLive()
        turnFlashOff()
//        session1 = nil
    }
    func turnFlashOn() {
        session1.torch = true
        flashButton.setImage(UIImage(named: "flashOff"), for: UIControlState.normal)
    }
    func turnFlashOff() {
        session1.torch = false
        flashButton.setImage(UIImage(named: "flashOn"), for: UIControlState.normal)

    }
    //MARK: - Live session delegates
    func liveSession(_ session: LFLiveSession?, debugInfo: LFLiveDebug?) {
        
    }
    func liveSession(_ session: LFLiveSession?, errorCode: LFLiveSocketErrorCode) {
    
    }
    func liveSession(_ session: LFLiveSession?, liveStateDidChange state: LFLiveState) {
        switch state {
        case .error:
            titleLabel.text = "Live Stream (Failed)"
            stopLive()
            break
        default:
            titleLabel.text = "Live Stream"
            break
        }
    }
    
    //MARK: - IBACTIONS
    @IBAction func switchCameraButtonPressed(_ sender: UIButton) {
        if session1.captureDevicePosition == .back {
            session1.captureDevicePosition = AVCaptureDevice.Position.front
            turnFlashOff()
        }
        else {
            session1.captureDevicePosition = AVCaptureDevice.Position.back
        }
    }
    @IBAction func backButtonPressed(_ sender: UIButton) {
        stopLive()
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func playPauseButtonPressed(_ sender: UIButton) {
        if isLive {
            stopLive()
            playPauseButton.setImage(UIImage(named: "play"), for: UIControlState.normal)
            isLive = false
        }
        else {
            startLive()
            playPauseButton.setImage(UIImage(named: "pause"), for: UIControlState.normal)
            isLive = true
        }
    }
    @IBAction func flashButtonPressed(_ sender: UIButton) {
        if session1.captureDevicePosition == AVCaptureDevice.Position.back {
            if session1.torch == true {
                turnFlashOff()
            }
            else {
                turnFlashOn()
            }
        }
        else {
            turnFlashOff()
        }
    }
    
    
}

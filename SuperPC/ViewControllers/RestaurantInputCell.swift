//
//  RestaurantInputCell.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 1/16/18.
//  Copyright © 2018 Muhammad Asif. All rights reserved.
//

import UIKit

@objc protocol RestaurantInputCellDelegate: class {
   
    func cellRestaurantNameTextFieldDidChange(_ tag: Int, textFieldText: String)
    func cellRestaurantNameTextFieldDidBeginEditing(_ tag: Int)
    func cellRestaurantNameTextFieldDidReturn(_ tag: Int)

    
}

class RestaurantInputCell: UITableViewCell, UITextFieldDelegate {

    //MARK: IBOUTLETS
    @IBOutlet weak var restaurantNameTextField: UITextField!
    
    //MARK: PROPERTIES
    weak var delegate:RestaurantInputCellDelegate?

    //MARK: CELL LIFE CYCLE
    override func awakeFromNib() {
        restaurantNameTextField.delegate = self
    }
    
    //MARK: TEXTFIELD DELEGATES
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.cellRestaurantNameTextFieldDidBeginEditing(self.tag)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.cellRestaurantNameTextFieldDidChange(self.tag, textFieldText: restaurantNameTextField.text!)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        delegate?.cellRestaurantNameTextFieldDidReturn(self.tag)
        return true
    }

    
}

//
//  ReportsViewController.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 12/27/17.
//  Copyright © 2017 Muhammad Asif. All rights reserved.
//

import UIKit

class ReportsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - IBOUTLETS
    @IBOutlet weak var reportsTableView: UITableView!
    
    //MARK: - PROPERTIES
    let reports: [Report] = [.sales, .void, .discount]
    var restaurant: Restaurant!
    var selectedReportIndex = -1
    
    //MARK: - VIEW CONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: TABLEVIEW DELGATES AND DATASOURCES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reports.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.MenuCell.rawValue) as! MenuCell
        let report = reports[indexPath.row]
        cell.menuTitleLabel.text = report.rawValue

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedReportIndex = indexPath.row
        performSegue(withIdentifier: SegueIdentifiers.ReportsVCToSalesReportVCSegue.rawValue, sender: self)
    }

    //MARK: - IBACTIONS
    @IBAction func backButtonPressed(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //MARK: - PREPARE FOR SEGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifiers.ReportsVCToSalesReportVCSegue.rawValue {
            let vc = segue.destination as! SalesReportViewController
            vc.restaurant = restaurant
            vc.report = reports[selectedReportIndex]
        }
    }
    
}

//
//  EmailViewController.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 1/3/18.
//  Copyright © 2018 Muhammad Asif. All rights reserved.
//

import UIKit
import SVProgressHUD

class EmailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    //MARK: - IBOULETS
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var attachmentsTableView: UITableView!
    @IBOutlet weak var emailBodyTextView: UITextView!

    //MARK: PROPERTIES
    var attachments: [Attachment] = []
    let imagePicker = UIImagePickerController()
    var menuType: Menu!
    var restaurant: Restaurant!
    var subject = ""
    
    //MARK: - VIEW CONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        if menuType == .submitMenuChanges {
            titleLabel.text = "Menu Changes"
            subject = "Menu Changes - \(restaurant.name)"
        }
        else {
            titleLabel.text = "Support Issue"
            subject = "Support Issue - \(restaurant.name)"

        }

    }

    //MARK: - SERVICE CALLS
    func sendEmail() {
        SVProgressHUD.show(withStatus: "Sending")
        weak var weakSelf = self
        let emailType = (menuType == .submitMenuChanges) ? "menu_changes":"support_issue"
        EmailService.sendEmail(emailType: emailType, senderEmail: UserService.persistedUser()!.email, subject: subject, body: emailBodyTextView.text, restaurantId: restaurant.streamingId, attachedImages: attachments.map{$0.image}) { (success, message, error) in
            SVProgressHUD.dismiss()
            if weakSelf != nil {
                if error != nil {
                    weakSelf!.presentOkAlert(error!.localizedErrorTitle(), message: error!.localizedErrorDescription())
                    return
                }
                if success! == true {
                    SVProgressHUD.showSuccess(withStatus: message!)
                    weakSelf!.navigationController?.popViewController(animated: true)
                    return
                }
                weakSelf!.presentOkAlert(AppStringConstants.BrandName, message: message!) {
                }
            }
            
        }
    }
    
    //MARK: VIEW HANDLING
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        endEditing()
        super.touchesBegan(touches, with:event)
    }
    func endEditing() {
        view.endEditing(true)
    }

    //MARK: TABLEVIEW DELGATES AND DATASOURCES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attachments.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.AttachmentCell.rawValue) as! AttachmentCell
        let attachment = attachments[indexPath.row]
        cell.attachmentNameLabel.text = attachment.name
        
        return cell
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.delete
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Delete"
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            attachments.remove(at: indexPath.row)
            attachmentsTableView.reloadData()
        }
    }

    //MARK: IMAGE PICKER CONTROLLER DELEGATES
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let attachment = Attachment(name: "Screenshot \(Date.init())", image: chosenImage)
        attachments.append(attachment)
        attachmentsTableView.reloadData()
        picker.dismiss(animated: true, completion: nil)

    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }

    //MARK: - IBACTIONS
    @IBAction func backButtonPressed(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func photoLibraryButtonPressed(_ sender: UIButton) {
        if CameraAndPhotoService.doesAppHavePhotosPermissions() {
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true, completion: nil)
            
        }
        else {
            presentConfirmation(AppStringConstants.BrandName, message: AppStringConstants.PhotosPermissionDeniedText, buttonTitle: "Go") { (confirmed) in
                if confirmed {
                    UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                }
            }
        }
    }
    @IBAction func cameraButtonPressed(_ sender: UIButton) {
        if CameraAndPhotoService.doesAppHaveCameraPermissions() {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            presentConfirmation(AppStringConstants.BrandName, message: AppStringConstants.CameraPermissionDeniedText, buttonTitle: "Go") { (confirmed) in
                if confirmed {
                    UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                }
            }
            
        }

    }
    @IBAction func sendEmailButtonPressed(_ sender: UIButton) {
        sendEmail()
    }
    

}

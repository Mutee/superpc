//
//  ForgotPasswordViewController.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 1/11/18.
//  Copyright © 2018 Muhammad Asif. All rights reserved.
//

import UIKit
import SVProgressHUD

class ForgotPasswordViewController: UIViewController, UITextFieldDelegate {

    //MARK: - IBOUTLETS
    @IBOutlet weak var emailTextField: UITextField!
    
    //MARK: - VIEW CONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - SERVICE CALLS
    func submit() {
        SVProgressHUD.show(withStatus: "Submitting")
        weak var weakSelf = self
        EmailService.sendEmail(emailType: "forgot_password", senderEmail: emailTextField.text!) { (success, message, error) in
            
            SVProgressHUD.dismiss()
            
            if weakSelf != nil {
                if error != nil {
                    if !weakSelf!.shouldHandleError(error: error!) {
                        return
                    }
                    weakSelf!.presentOkAlert(error!.localizedErrorTitle(), message: error!.localizedErrorDescription())
                    return
                }
                weakSelf!.presentOkAlert(AppStringConstants.BrandName, message: message!) {
                    if success! == true {
                        weakSelf?.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    //MARK: TEXTFIELD DELEGATES
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: IBACTIONS
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        
        //Validate for empty fields
        if emailTextField.text == "" {
            
            presentOkAlert(AppStringConstants.BrandName, message: AppStringConstants.EmptyFieldsMessage)
            return
        }
        
        //Check for valid email address
        if !emailTextField.text!.isValidEmail() {
            
            presentOkAlert(AppStringConstants.BrandName, message: AppStringConstants.InvalidEmailMessage)
            return
        }
        
        
        //Valid input. Send to server
        submit()
        
    }
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }

}

//
//  LoginViewController.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 12/19/17.
//  Copyright © 2017 Muhammad Asif. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginViewController: UIViewController, UITextFieldDelegate {

    //MARK: - IBOUTLETS
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    //MARK: - VIEW CONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        emailTextField.text = ""
        passwordTextField.text = ""
    }
    //MARK: SERVICE CALLS
    func login() {
        weak var weakSelf = self
        SVProgressHUD.show(withStatus: "Signing In")
        UserService.signIn(emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            SVProgressHUD.dismiss()
            if weakSelf != nil {
                if error != nil {
                    weakSelf!.presentOkAlert(error!.localizedErrorTitle(), message: error!.localizedErrorDescription())
                    return
                }
                
                weakSelf!.performSegue(withIdentifier: SegueIdentifiers.LoginVCToRestaurantsVCSegue.rawValue, sender: weakSelf!)
            }
        }
    }
 
    //MARK: TEXTFIELD DELEGATES
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - IBACTIONS
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        if emailTextField.text == "" || passwordTextField.text == "" {
            presentOkAlert(AppStringConstants.LoginAlertsTitle, message: AppStringConstants.EmptyFieldsMessage)
            return
        }
        if !emailTextField.text!.isValidEmail() {
            presentOkAlert(AppStringConstants.LoginAlertsTitle, message: AppStringConstants.InvalidEmailMessage)
            return
        }
        login()
    }
}

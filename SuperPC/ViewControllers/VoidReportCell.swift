//
//  VoidReportCell.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 3/2/18.
//  Copyright © 2018 Muhammad Asif. All rights reserved.
//

import UIKit

class VoidReportCell: UITableViewCell {

    @IBOutlet weak var voidAmountLabel: UILabel!
    @IBOutlet weak var employeeLabel: UILabel!
    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var voidReasonLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!

}

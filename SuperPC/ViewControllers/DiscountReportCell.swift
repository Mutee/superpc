//
//  DiscountReportCell.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 3/2/18.
//  Copyright © 2018 Muhammad Asif. All rights reserved.
//

import UIKit

class DiscountReportCell: UITableViewCell {

    @IBOutlet weak var discountAmountLabel: UILabel!
    @IBOutlet weak var discountNameLabel: UILabel!
    @IBOutlet weak var employeeNameLabel: UILabel!
    @IBOutlet weak var orderNoLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
}

//
//  CALayer+UWGLGenericAppSolution.swift
//  UWGGenericAppSolution
//
//  Created by Mutee ur Rehman on 11/14/16.
//  Copyright © 2016 United Wholesale Grocers Limited. All rights reserved.
//

import Foundation
import UIKit

extension CALayer {
    var borderUIColor: UIColor {
        set {
            self.borderColor = newValue.cgColor
        }
        
        get {
            return UIColor(cgColor: self.borderColor!)
        }
    }
}

//
//  NSError+UWGLGenericAppSolution.swift
//  UWGGenericAppSolution
//
//  Created by Mutee ur Rehman on 1/18/17.
//  Copyright © 2017 United Wholesale Grocers Limited. All rights reserved.
//

import Foundation

private var _genricApplicationErrorDomain = Bundle.main.bundleIdentifier ?? "GenericApplicationErrorDomain"
let UWGInnerErrorDescriptionKey = "InnerErrorDescription"
let UWGLocalizedErrorTitleKey = "LocalizedErrorTitle"

extension NSError {
    
    /// Configure application error domain
    class var genricApplicationErrorDomain: String {
        get {
            return _genricApplicationErrorDomain
        }
        set {
            _genricApplicationErrorDomain = genricApplicationErrorDomain
        }
    }
    
    convenience init(domain: String = NSError.genricApplicationErrorDomain, code: Int = -1, title: String = "", description: String = "", failureReason: String = "") {
        let userInfo = [
            NSLocalizedDescriptionKey: description,
            NSLocalizedFailureReasonErrorKey: failureReason,
            UWGInnerErrorDescriptionKey: description,
            UWGLocalizedErrorTitleKey: title
        ]
        self.init(domain: domain, code: code, userInfo: userInfo)
    }
    
    convenience init(uwgError: UWGError, code: Int = -1, innerError: NSError? = nil) {
        let localizedDescription = uwgError.localizedErrorMessage()
        let localizedTitle = uwgError.localizedErrorTitle()
        
        let userInfo = [
            NSLocalizedDescriptionKey: localizedDescription,
            UWGLocalizedErrorTitleKey: localizedTitle,
            UWGInnerErrorDescriptionKey: innerError?.fullDescription() ?? ""
        ]
        self.init(domain: uwgError.domain(), code: code, userInfo: userInfo)
    }
    
    func localizedErrorTitle() -> String {
        return (userInfo[UWGLocalizedErrorTitleKey] as? String) ?? ""
    }
    func localizedErrorDescription() -> String {
        return (userInfo[NSLocalizedDescriptionKey] as? String) ?? ""
    }

    func innerErrorDescription() -> String {
        return (userInfo[UWGInnerErrorDescriptionKey] as? String) ?? ""
    }
    
    fileprivate func fullDescription() -> String {
        return "----------\nDomain: \(domain)\nCode:\(code)\nDescription:\(description)\nLocalized Description:\(localizedDescription)\nFailure Reason:\(localizedFailureReason)\n Inner Error Description:\(innerErrorDescription())\n**********"
    }
    


}

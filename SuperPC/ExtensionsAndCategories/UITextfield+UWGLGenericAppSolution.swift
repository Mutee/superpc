//
//  UITextfieldExtension.swift
//  UWGGenericAppSolution
//
//  Created by Mutee ur Rehman on 4/20/16.
//  Copyright © 2016 United Wholesale Grocers Limited. All rights reserved.
//

import Foundation
import UIKit

extension UITextField
{
    
    func addToolBarWithButtons(_ leftButtonTitle:String, rightButtonTitle:String, leftButtonAction:Selector, rightButtonAction:Selector, toolBarBackgroundColor:UIColor, toolBarTintColor:UIColor, target:AnyObject)
    {
        
        let leftButton = UIBarButtonItem(title: leftButtonTitle, style: UIBarButtonItemStyle.done, target: target, action: leftButtonAction)
        leftButton.tag = self.tag
        
        let rightButton = UIBarButtonItem(title: rightButtonTitle, style: UIBarButtonItemStyle.done, target: target, action: rightButtonAction)
        rightButton.tag = self.tag
       
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)

        
        let keyBoardToolBar = UIToolbar()
        keyBoardToolBar.barStyle = UIBarStyle.default
        keyBoardToolBar.isTranslucent = true
        keyBoardToolBar.tintColor = toolBarTintColor
        keyBoardToolBar.setItems([leftButton, spaceButton, rightButton], animated: false)
        keyBoardToolBar.isUserInteractionEnabled = true
        keyBoardToolBar.sizeToFit()
        
        self.inputAccessoryView = keyBoardToolBar


    }
    
    @IBInspectable var placeholderColor: UIColor {
        get {
            
            return UIColor.init()
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                            attributes: [NSAttributedStringKey.foregroundColor: newValue])
        }
    }

    func setPlaceHolder(text: String, color: UIColor) {
        self.attributedPlaceholder = NSAttributedString(string: text,
                                                        attributes: [NSAttributedStringKey.foregroundColor: color])
    }

}

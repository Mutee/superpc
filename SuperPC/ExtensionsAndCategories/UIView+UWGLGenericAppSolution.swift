//
//  UIView+UWGLGenericAppSolution.swift
//  UWGGenericAppSolution
//
//  Created by Mutee ur Rehman on 6/19/17.
//  Copyright © 2017 United Wholesale Grocers Limited. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    func findFirstResponder() -> UIView? {
        for subView in self.subviews {
            if subView.isFirstResponder {
                return subView
            }
            
            if let recursiveSubView = subView.findFirstResponder() {
                return recursiveSubView
            }
        }
        
        return nil
    }

}


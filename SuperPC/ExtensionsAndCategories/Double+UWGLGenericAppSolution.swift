//
//  DoubleExtension.swift
//  UWGGenericAppSolution
//
//  Created by Mutee ur Rehman on 6/6/16.
//  Copyright © 2016 United Wholesale Grocers Limited. All rights reserved.
//

import Foundation

public extension Double {
    func roundToPlaces(_ places:Int) -> String {
        let divisor = pow(10.0, Double(places))
        let roundedDouble =  (self * divisor).rounded() / divisor
        return String(format: "%.2f", roundedDouble)
    }
    func stringWithThousandSeperator() -> String {
        return NumberFormatter.localizedString(from: NSNumber(value: self), number: NumberFormatter.Style.decimal)

    }

}

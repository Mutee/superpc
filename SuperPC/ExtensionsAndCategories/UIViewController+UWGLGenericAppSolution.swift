//
//  UiViewControllerExtension.swift
//  UWGGenericAppSolution
//
//  Created by Mutee ur Rehman on 4/26/16.
//  Copyright © 2016 United Wholesale Grocers Limited. All rights reserved.
//

import Foundation
import UIKit

//MARK: ALERTS
extension UIViewController
{

    /// Present alert with multiple actions
    public func presentAlert(_ title: String, message: String, actions: UIAlertAction..., preferredStyle: UIAlertControllerStyle  = .alert) {
        if actions.count == 0 {
            presentOkAlert(title, message: message)
            return
        }
        let alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        for action in actions {
            alertController.addAction(action)
        }
        present(alertController, animated: true, completion: nil)
    }
    
    /// Present single button alert with callback over current controller
    public func presentAlert(_ title: String, message: String, buttonTitle: String, callback: (() -> Void)? = nil) {
        let action = UIAlertAction(title: buttonTitle, style: .cancel) { action in
            callback?()
        }
        presentAlert(title, message: message, actions: action)
    }
    
    /// Present single Ok button alert with callback over current controller
    public func presentOkAlert(_ title: String, message: String, callback: (() -> Void)? = nil) {
        presentAlert(title, message: message, buttonTitle: "Okay", callback: callback)
    }
    
    /// Present single button error alert with callback over current controller
    public func presentError(_ error: NSError?, callback: (() -> Void)? = nil) {
        if error != nil {
            presentAlert("Error", message: error!.localizedDescription, buttonTitle: "Okay", callback: callback)
        }
    }
    
    /// Present two button alert with callback over current controller
    public func presentConfirmation(_ title: String, message: String, buttonTitle: String, cancelButtonTitle: String = "Cancel", callback: @escaping (_ confirmed: Bool) -> Void) {
        let okAction = UIAlertAction(title: buttonTitle, style: .default) { action in
            callback(true)
        }
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel) { action in
            callback(false)
        }
        presentAlert(title, message: message, actions: okAction, cancelAction)
    }
}

//MARK: SERVICE ERROR HANDLING
extension UIViewController {
    func shouldHandleError(error: NSError) -> Bool {
        return true
    }
}

//MARK: SHADOW VIEW
extension UIViewController {
    func fadeView() {
        view.layer.opacity = 0.3
        view.layer.backgroundColor = UIColor.black.cgColor
        view.isUserInteractionEnabled = false
    }
    func clearShadow() {
        view.layer.opacity = 1.0
        view.layer.backgroundColor = UIColor.white.cgColor
        view.isUserInteractionEnabled = true
    }
}

//
//  StringExtension.swift
//  UWGGenericAppSolution
//
//  Created by Mutee ur Rehman on 4/26/16.
//  Copyright © 2016 United Wholesale Grocers Limited. All rights reserved.
//

import Foundation

public extension String{
    
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }

    // Email validation
    func isValidEmail() -> Bool {
        
        let emailRegex = "[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}"
        
        return matchWithRegex(emailRegex)
    }
    // if string matches with parameter regex string
    func matchWithRegex(_ regexString: String) -> Bool
    {
        let predicate = NSPredicate(format: "SELF MATCHES %@"
            ,regexString )
        let isValid = predicate.evaluate(with: self)
        
        return isValid;
        
    }
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }

    func abbreviation() -> String {
        var firstCharacters : String = ""
        let words = components(separatedBy: CharacterSet.whitespaces)
        for word in words {
            if word.characters.count > 0 {
                let firstLetter = (word as NSString).substring(to: 1)
                firstCharacters =  firstCharacters + firstLetter.uppercased()
            }
        }
        return firstCharacters;
        
    }
    //Returns false if string contains any charatcter other than alphabets(Aa-Zz).
    func containsOnlyLetters() -> Bool {
        for chr in self.characters {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z") && chr != " " ) {
                return false
            }
        }
        return true
    }
    func containsNumericCharacters() -> Bool {
        
        let decimalCharacters = CharacterSet.decimalDigits
        
        let decimalRange = self.rangeOfCharacter(from: decimalCharacters)
        
        if decimalRange != nil {
            return true
        }
        return false
    }
    func containsLetters() -> Bool {
        for chr in self.characters {
            if ((chr >= "a" && chr <= "z") || (chr >= "A" && chr <= "Z")) {
                return true
            }
        }
        return false
    }
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    var intValue: Int {
        return Int((self as NSString).intValue)
    }


}

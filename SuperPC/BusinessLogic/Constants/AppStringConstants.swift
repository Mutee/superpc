//
//  AppStringConstants.swift
//  UWGGenericAppSolution
//
//  Created by Mutee ur Rehman on 4/28/16.
//  Copyright © 2016 United Wholesale Grocers Limited. All rights reserved.
//

import Foundation

struct AppStringConstants {
   
    static let APIBaseURLString = ""
    static let BrandName = "SuperPC"
    
    
    static let SelectPhotoFromLibraryText = "Select A Photo"
    static let TakeNewPhotoText = "Take New Photo"

    static let PhotosPermissionDeniedText = "It looks like your privacy settings are preventing us from accessing your Photo Library. \(AppStringConstants.BrandName) will only upload the photos you choose. You can fix this by doing the following:\n\r1. Touch Go to open Settings app.\n2. Touch Privacy to inners Setting.\n3. Change Photos Settings as ON.\n4. Open \(AppStringConstants.BrandName) App and Try again!"
    static let CameraPermissionDeniedText = "It looks like your privacy settings are preventing us from accessing your Camera to take photos. \(AppStringConstants.BrandName) will only upload the photos you choose. You can fix this by doing the following:\r\n\n1. Touch Go to open Settings app.\n2. Touch Privacy to inners Setting.\n3. Change Camera Settings as on.\n4. Open \(AppStringConstants.BrandName) App and Try again!"

    static let SupportPhoneNumber = "(718) 333-0999"
    static let PasswordsDontMatch = "Passwords do not match."
    //Login Strings
    static let LoginAlertsTitle = "Login"
    static let InValidLoginCredentialsMessage = "Your login credentials are invalid! Please check and try again"

    //Generic Strings
    static let SuccessTitle = "Success"
    static let NetworkFailureTitle = "Network Failure"
    static let EmptyFieldsMessage = "One or more required fields are incomplete. Please fill in the remaining fileds to continue."

    static let InvalidEmailMessage = "Please enter a valid email address."
    static let InvalidPassword = "Password must be atleast 7 characters long and contains atleast one letter and one number."
    static let NetworkFailureMessage = "Operation failed beacuse connection to internet has been lost. Please connect to your nearest Wifi hotspot or network to Retry!"
    static let KeyBoardDoneButtonText = "Done"
    static let KeyBoardClearButtonText = "Clear"
    static let KeyBoardCancelButtonText = "Cancel"
    static let KeyBoardNextButtonText = "Next"
    static let KeyBoardAddButtonText = "Add"
    
    static let ServerFailureText = "Server side database failed due to unknown reasons. Please try again. Please contact admin if problem persists."
    static let ServerFailureTitle = "Server Failure"

    //DB Error 
    static let UnexpectedErrorMessage = "Unexpected Error"

}

//
//  AppEnumerations.swift
//  UWGGenericAppSolution
//
//  Created by Mutee ur Rehman on 8/10/16.
//  Copyright © 2016 United Wholesale Grocers Limited. All rights reserved.
//

import Foundation
import UIKit

enum Notifications: String {
    case UserLoggedOut
    
    var notificationName: Notification.Name {
        get {
            return Notification.Name(rawValue: self.rawValue)
        }
    }
}
enum Menu: String {
    case reports = "Reports"
    case order = "Order Paper And Ink"
    case submitMenuChanges = "Submit Menu Changes"
    case submitSupportIssue = "Submit Support Issue"
    case videoStreaming = "Enable Video Streaming"

}
enum Product: String {
    case thermalPaperForReceiptPaper = "Thermal Paper For Receipt Paper"
    case onePlyPapaerForKitchenPrinter = "1-Ply Paper For Kitchen Printer"
    case twoPlyPapaerForKitchenPrinter = "2-Ply Paper For Kitchen Printer"
    case threePlyPapaerForKitchenPrinter = "3-Ply Paper For Kitchen Printer"
    case twelwePackOfInkCartridgesSPS = "12-pack of Ink Cartridges (SPS)"
    case twelwePackOfInkCartridgesSNBC = "12-pack of Ink Cartridges (SNBC)"
    case twelwePackOfInkCartridgesEpson = "12-pack of Ink Cartridges (Epson)"
    case twelwePackOfInkCartridgesStar = "12-pack of Ink Cartridges (Star)"
    case twelwePackOfInkCartridgesOther = "12-pack of Ink Cartridges (Other)"
    
    var rawValueForServer: String {
        switch self {
        case .twelwePackOfInkCartridgesSPS:
            return "12-pack of Ink Cartridges"
        case .twelwePackOfInkCartridgesSNBC:
            return "12-pack of Ink Cartridges"
        case .twelwePackOfInkCartridgesEpson:
            return "12-pack of Ink Cartridges"
        case .twelwePackOfInkCartridgesStar:
            return "12-pack of Ink Cartridges"
        case .twelwePackOfInkCartridgesOther:
            return "12-pack of Ink Cartridges"
        default:
            return rawValue
        }
    }
    var printerBrand: String? {
        switch self {
        case .twelwePackOfInkCartridgesSPS:
            return "SPS"
        case .twelwePackOfInkCartridgesSNBC:
            return "SNBC"
        case .twelwePackOfInkCartridgesEpson:
            return "Epson"
        case .twelwePackOfInkCartridgesStar:
            return "Star"
        case .twelwePackOfInkCartridgesOther:
            return "Other"
        default:
            return nil
        }
    }
    var isInkCartidge: Bool {
        return self == .twelwePackOfInkCartridgesSPS || self == . twelwePackOfInkCartridgesSNBC || self == .twelwePackOfInkCartridgesEpson || self == .twelwePackOfInkCartridgesStar || self == .twelwePackOfInkCartridgesOther
    }
}
enum Report: String {
    case sales = "Sales Report"
    case void = "Void Report"
    case discount = "Discount Report"
    
    var reportNameForServer: String {
        switch self {
        case .sales:
            return "sales"
        case .void:
            return "voids"
        case .discount:
            return "discounts"
        }
    }
    var placeholder: String {
        switch self {
        case .sales:
            return ""
        case .void:
            return "No Voids for selected timeframe"
        case .discount:
            return "No Discounts for selected timeframe"
        }
    }
}
enum SegueIdentifiers: String {
    case LoginVCToRestaurantsVCSegue = "LoginVCToRestaurantsVCSegue"
    case LoginVCToMenuVCSegue = "LoginVCToMenuVCSegue"
    case RestaurantsVCToMenuVCSegue = "RestaurantsVCToMenuVCSegue"
    case ReportsVCToSalesReportVCSegue = "ReportsVCToSalesReportVCSegue"
    case RestaurantsVCToLiveStreamVCSegue = "RestaurantsVCToLiveStreamVCSegue"
    case RestaurantsVCToReportsVCSegue = "RestaurantsVCToReportsVCSegue"
    case RestaurantsVCToEmailVCSegue = "RestaurantsVCToEmailVCSegue"
    case RestaurantsVCToOrderVCSegue = "RestaurantsVCToOrderVCSegue"
}
enum CellIdentifiers: String {
    case RestaurantCell = "RestaurantCell"
    case MenuCell = "MenuCell"
    case AttachmentCell = "AttachmentCell"
    case ProductCell = "ProductCell"
    case InkCartridgeCell = "InkCartridgeCell"
    case RestaurantInputCell = "RestaurantInputCell"
    case AddRestauarntCell = "AddRestauarntCell"
    case VoidReportCell = "VoidReportCell"
    case DiscountReportCell = "DiscountReportCell"
    
}
enum UserDefaultsKeys: String {
    case UserInfo = "UserInfo"
}
enum DiscountType {
    case order
    case item
    case cash
}
enum UWGError: Int {
    case networkFailure = 0
    case invalidLoginCredentials = 1
    case unexpectedError = 2
    case unknownAPIResponseFormat = 3
    
    func domain() -> String {
        let prefix = Bundle.main.bundleIdentifier ?? "SuperPC"
        switch self {
        case .networkFailure:
            return "\(prefix).NetworkFailure"
        case .invalidLoginCredentials:
            return "\(prefix).InValidLoginCredentials"
        case .unexpectedError:
            return "\(prefix).UexpectedError"
        case .unknownAPIResponseFormat:
            return "\(prefix).UnknownAPIResponseFormat"
        }
    }
  
    func localizedErrorMessage() -> String {
        switch self {
        case .networkFailure:
            return AppStringConstants.NetworkFailureMessage
        case .invalidLoginCredentials:
            return AppStringConstants.InValidLoginCredentialsMessage
        case .unexpectedError:
            return AppStringConstants.UnexpectedErrorMessage
        case .unknownAPIResponseFormat:
            return AppStringConstants.ServerFailureText
            
        }
    }
   
    func localizedErrorTitle() -> String {
        switch self {
        case .networkFailure:
            return AppStringConstants.NetworkFailureTitle
        case .invalidLoginCredentials:
            return AppStringConstants.LoginAlertsTitle
        case .unexpectedError:
            return AppStringConstants.BrandName
        case .unknownAPIResponseFormat:
            return AppStringConstants.ServerFailureTitle
        }
    }
    
}


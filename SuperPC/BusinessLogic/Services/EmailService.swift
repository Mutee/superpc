//
//  EmailService.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 1/3/18.
//  Copyright © 2018 Muhammad Asif. All rights reserved.
//

import Foundation

typealias EmailServiceCallback = (_ success: Bool?, _ message: String?, _ error: NSError?) -> Void

class EmailService {
    
    class func sendEmail(emailType: String, senderEmail: String = "", subject: String = "", body: String = "", restaurantId: String = "", attachedImages: [UIImage] = [], cartProductsForOrder: [CartProduct] = [], kitchenPrinterBrand: String = "", emailAddressOfNewUser: String = "", nameOfNewUser: String = "", newRestaurants: [String] = [], password: String = "", callback: @escaping EmailServiceCallback) {
       
        let orderProductsData = try! JSONSerialization.data(withJSONObject: cartProductsForOrder.map{$0.serializeAsJSONDictionary()}, options: .prettyPrinted)
        
        let orderProductsJsonString = String(data: orderProductsData, encoding: .utf8)!
        
        let restaurantNamesData = try! JSONSerialization.data(withJSONObject: newRestaurants.map{JSONDictonary.init(dictionaryLiteral: ("restaurant_name", $0 as AnyObject))}, options: .prettyPrinted)
        
        let restaurantNamesJsonString = String(data: restaurantNamesData, encoding: .utf8)!

        let bodyParams: JSONDictonary = ["email_type": emailType as AnyObject, "sender": senderEmail as AnyObject, "subject": subject as AnyObject, "body": body as AnyObject, "restaurant_id": restaurantId as AnyObject, "order_items":orderProductsJsonString  as AnyObject, "kitchen_printer_brand": kitchenPrinterBrand as AnyObject, "name": nameOfNewUser as AnyObject, "restaurant_names": restaurantNamesJsonString as AnyObject, "email_address": emailAddressOfNewUser as AnyObject, "password": password as AnyObject]
       
        APIService.upload("http://crm.pcsuper.com/index.php?entryPoint=sendEmail", bodyParameters: bodyParams, images: attachedImages) { (jsonResponse, error) in
            
            if error != nil {
                callback(nil, nil, error)
                return
            }
            callback(jsonResponse["status"].boolValue, jsonResponse["message"].stringValue, nil)
        }

    }
}

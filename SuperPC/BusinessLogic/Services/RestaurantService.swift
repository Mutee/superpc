//
//  RestaurantService.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 12/19/17.
//  Copyright © 2017 Muhammad Asif. All rights reserved.
//

import Foundation
import SwiftyJSON

class RestaurantService {
    class func getRestaurantStatus(callback: @escaping ErrorCallback) {
        
        let commaSeperatedPhones = UserService.persistedUser()!.restaurants.map{$0.businessPhone}.joined(separator: ",")

        let queryStringParameter: JSONDictonary = [:]
        APIService.get("http://spc.yury.com/api/stat/\(commaSeperatedPhones)", queryStringParameters: queryStringParameter) { (jsonResponse, error) in
            if error != nil {
                callback(error)
                return
            }
            let restaurants = UserService.persistedUser()!.restaurants
            var updatedRestaurants: [Restaurant] = []
            let statusesJsonArray = jsonResponse.arrayValue
            
            for restaurant in restaurants {
                let updatedRestaurant = restaurant
                for status in statusesJsonArray {
                    let st = APIService.parseBody(status.stringValue)
                    if st["phone"].stringValue == restaurant.businessPhone {
                        if st["stat"].stringValue == "disconnected" {
                            updatedRestaurant.isActive = false
                        }
                        else {
                            updatedRestaurant.isActive = true
                        }
                    }
                }
                updatedRestaurants.append(updatedRestaurant)
            }
            let user = UserService.persistedUser()!
            user.restaurants = updatedRestaurants
            UserService.saveUser(user)
            callback(nil)
        }
    }
}

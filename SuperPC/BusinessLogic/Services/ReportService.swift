//
//  ReportService.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 12/20/17.
//  Copyright © 2017 Muhammad Asif. All rights reserved.
//

import Foundation
import SwiftyJSON

typealias ReportsCallback = (_ totalSales: Double?, _ voids: [VoidReport]?, _ discounts: [Discount]?, _ error: NSError?) -> Void
class ReportService {
    
    class func getReport(reportType: Report, businessPhone: String, startDateString: String, endDateString: String, callback: @escaping ReportsCallback) {
      
        let queryStringParameters: JSONDictonary = ["q": reportType.reportNameForServer as AnyObject, "from_date": startDateString as AnyObject, "to_date": endDateString as AnyObject]
        let urlString = "http://spc.yury.com/api/qry/\(businessPhone)"
       
        APIService.get(urlString, queryStringParameters: queryStringParameters) { (jsonResponse, error) in
            if error != nil {
                callback(nil, nil, nil, error)
                return
            }
            if reportType == .sales {
                
                if jsonResponse["totalsales"] == JSON.null {
                    callback(nil, nil, nil, nil)
                }
                else {
                    callback(jsonResponse["totalsales"].doubleValue, nil, nil, nil)
                }
            }
            else if reportType == .void {
                let voids = jsonResponse.arrayValue.map{VoidReport(json: $0)}
                callback(nil, voids, nil, nil)
            }
            else {
                let orderDiscounts = jsonResponse["orderdisc"].arrayValue.map{Discount(json: $0, type: .order)}
                
                let itemDiscounts = jsonResponse["itemdisc"].arrayValue.map{Discount(json: $0, type: .item)}

                let cashDiscounts = jsonResponse["cashdisc"].arrayValue.map{Discount(json: $0, type: .cash)}

                let discounts = orderDiscounts + itemDiscounts + cashDiscounts
                callback(nil, nil, discounts, nil)
            }
            
        }
    }
    
        

}

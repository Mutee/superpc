
//
//  UserService.swift
//  UWGGenericAppSolution
//
//  Created by Taha Samad on 4/11/16.
//  Copyright © 2016 United. All rights reserved.
//

import Foundation
import SwiftyJSON

typealias UserCallback = (_ user: User?, _ error: NSError?) -> Void
typealias RegisterCallBack = (_ success: Bool?, _ error: NSError?) -> Void
typealias ErrorCallback = (_ error: NSError?) -> Void

class UserService {
    
    private static var user:User?

    class func signIn(_ emailAddress: String, password: String, callback: @escaping UserCallback) {
        let bodyParams: JSONDictonary = ["email": emailAddress as AnyObject, "password": password as AnyObject, "entryPoint": "mobileAccess" as AnyObject]

        APIService.post("http://crm.pcsuper.com/index.php", bodyParameters: bodyParams) { (jsonResponse, error) in
            if error != nil {
                callback(nil, error)
                return
            }
            let loginStatus = jsonResponse["status"].stringValue
            
            if jsonResponse["status"].boolValue != true {
                let err = NSError(uwgError: .invalidLoginCredentials)
                callback(nil, err)
            }
            else {
                
                var modifiedJsonResponse = jsonResponse
                modifiedJsonResponse["email"].stringValue = emailAddress
                modifiedJsonResponse["password"].stringValue = password
                let user = User(json: modifiedJsonResponse)
                saveUser(user)
                
                RestaurantService.getRestaurantStatus() { (error) in
                    callback(UserService.persistedUser()!, nil)
                }
            }
        }
    }
    class func logout() {
        UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.UserInfo.rawValue)
        UserDefaults.standard.synchronize()
        user = nil
    }
   
    //Save User Object To NSUserDefaults as JSONDictionary
    class func saveUser(_ user:User)
    {
        let standardUserDefaults = UserDefaults.standard
        
        let userJsonDictionary:JSONDictonary = user.serializeAsJSONDictionary()
        standardUserDefaults.set(userJsonDictionary, forKey:UserDefaultsKeys.UserInfo.rawValue)
        standardUserDefaults.synchronize()
        self.user = user
    }
   
    //Returns Current User Object. Returns Nil if no user is signed in
    class func persistedUser() -> User?
    {
        let standardUserDefaults = UserDefaults.standard
        
        if (standardUserDefaults.dictionary(forKey: UserDefaultsKeys.UserInfo.rawValue) != nil && self.user == nil)
        {
            let userDictionary:JSONDictonary = standardUserDefaults.dictionary(forKey: UserDefaultsKeys.UserInfo.rawValue)! as JSONDictonary
            let json = JSON.init(userDictionary)
            self.user = User(json: json)
            return self.user

        }
        return self.user
    }

}

//
//  CameraAndPhotoService.swift
//  UWGGenericAppSolution
//
//  Created by Mutee ur Rehman on 5/16/16.
//  Copyright © 2016 United Wholesale Grocers Limited. All rights reserved.
//

import Foundation
import AVFoundation
import Photos

class CameraAndPhotoService {
    class func doesAppHaveCameraPermissions() -> Bool {
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized
        {
            return true
        }
        return false
    }
    class func askForCameraPermissionIfNeeded() -> Void {
    
        AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted :Bool) -> Void in
            if granted == true
            {
                //permission granted
            }
            else
            {
                //permission denied
            }
        });

    }
    class func doesAppHavePhotosPermissions() -> Bool {
        
        let status:PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if status == PHAuthorizationStatus.authorized
        {
            return true
        }
        return false

    }
    class func askForPhotosLibraryPermissiosn() -> Void {
        PHPhotoLibrary.requestAuthorization() { (authorizationStatus: PHAuthorizationStatus) -> Void in
            
            if authorizationStatus == PHAuthorizationStatus.authorized {
                //permission granted
            }
            else {
                //permission denied
            }
        }
    }
}

//
//  APIService.swift
//  UWGGenericAppSolution
//
//  Created by Taha Samad on 4/11/16.
//  Copyright © 2016 United. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public typealias JSONDictonary = [String: AnyObject]
public typealias APIServiceCallback = (_ jsonResponse: JSON, _ error: NSError?) -> Void
public typealias ProgressCallback = (_ fractionCompleted: Double?) -> Void

class APIService {
    
    static fileprivate(set) var baseURL = "";
    
    class func configurationForService(_ APIBaseURL: String) {
        baseURL = APIBaseURL;
    }
    
    class func get(_ path: String, queryStringParameters: JSONDictonary? = nil, callback: @escaping APIServiceCallback) {
        sendRequest(.get, path: path, queryStringParameters: queryStringParameters, callback: callback)
    }
    
    class func post(_ path: String, bodyParameters: JSONDictonary, queryStringParameters: JSONDictonary? = nil, callback: @escaping APIServiceCallback) {
        sendRequest(.post, path: path, bodyParameters: bodyParameters, callback: callback)
    }
    class func download(_ path: String, bodyParameters: JSONDictonary? = nil, queryStringParameters: JSONDictonary? = nil, callback: @escaping APIServiceCallback, progressCallback: @escaping ProgressCallback) {
        download(.get,  path: path, bodyParameters: bodyParameters, queryStringParameters: queryStringParameters, callback: callback, progressCallback: progressCallback)
    }
    class func put(_ path: String, bodyParameters: JSONDictonary, queryStringParameters: JSONDictonary? = nil, callback: @escaping APIServiceCallback) {
        sendRequest(.put, path: path, bodyParameters: bodyParameters, callback: callback)
    }
    
    class func delete(_ path: String, bodyParameters: JSONDictonary? = nil, queryStringParameters: JSONDictonary? = nil, callback: @escaping APIServiceCallback) {
        sendRequest(.delete, path: path, bodyParameters: bodyParameters, callback: callback)
    }
    class func upload(_ path: String, bodyParameters: JSONDictonary? = nil, images: [UIImage], callback: @escaping APIServiceCallback ) {
        
        // Build url
        let urlString = self.buildAPIURL(path)
        
        // Make Request
        let URL = Foundation.URL(string: urlString)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            var count = 0
            //Convert images to data and append to mutlipartFormData
            for image in images {
               
                if let imageData = UIImageJPEGRepresentation(image, 0.5) {
                    multipartFormData.append(imageData, withName: "userfile\(count)", fileName: "image.jpg", mimeType: "image/jpg")
                    count += 1
                }
            }
            if bodyParameters != nil {
                //Convert parametrs into data and append to mutlipartFormData
                for (key, value) in bodyParameters! {
                    
                    let valueString = value as! String
                    let data = valueString.data(using: String.Encoding.utf8)!
                    multipartFormData.append(data, withName: key)
                    
                }

            }
            }, to: URL!, encodingCompletion: { (result) in
                switch result {
               
                //Successfuly encoded
                case .success(let upload, _, _):
                   
                    upload.responseString { alamofireResponse in
                       
                        //handle response
                        handleResponse(alamofireResponse: alamofireResponse, callback: callback)
                        
                    }
                case .failure(let encodingError):
                 
                    //Encoding failed
                    print(encodingError)
                    
                }
        } )
    }
    private class func download(_ method: Alamofire.HTTPMethod, path: String, bodyParameters: JSONDictonary? = nil, queryStringParameters: JSONDictonary? = nil, callback: @escaping APIServiceCallback, progressCallback: @escaping ProgressCallback) {
       
        // Build url
        let urlString = self.buildAPIURL(path)
        
        // Make Request
        let URL = Foundation.URL(string: urlString)

        var parameters: JSONDictonary?
        var encoding: ParameterEncoding = URLEncoding.queryString
       
        //encode body paramters
        if bodyParameters != nil {
            parameters = bodyParameters
            encoding = JSONEncoding.default
        }
        //encode URL Query String Parameters
        if queryStringParameters != nil {
            parameters = queryStringParameters
            encoding = URLEncoding.queryString
        }
        
//        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent("AllProducts")
            
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }

        Alamofire.download(URL!, method: method, parameters: parameters, encoding: encoding, headers: nil, to: destination)
            .downloadProgress(queue: DispatchQueue.main) { progress in
                progressCallback(progress.fractionCompleted)
            }
            .responseString(encoding: String.Encoding.utf8){ alamofireResponse in
                let req = alamofireResponse.request
                let result = alamofireResponse.result
                let res = alamofireResponse.response
                let error = alamofireResponse.result.error
                
                    self.handleResponse(req!, response: res, body: result.value, error: error as NSError?, callback: callback)
                }
            }
    private class func sendRequest(_ method: Alamofire.HTTPMethod , path: String, bodyParameters: JSONDictonary? = nil, queryStringParameters: JSONDictonary? = nil, callback: @escaping APIServiceCallback) {
        
        // Build url
        let urlString = self.buildAPIURL(path)
        
        // Make Request
        let URL = Foundation.URL(string: urlString)
        //  var mutableURLRequest = NSMutableURLRequest(url: URL!)
        
        var mutableURLRequest: DataRequest!
        
        //encode body paramters
        if bodyParameters != nil {
            mutableURLRequest = Alamofire.request(URL!, method: method, parameters: bodyParameters, encoding: URLEncoding.httpBody)
        }
        
        //encode URL Query String Parameters
        if queryStringParameters != nil {
            mutableURLRequest = Alamofire.request(URL!, method: method, parameters: queryStringParameters, encoding: URLEncoding.queryString)
        }
        
        // Add headers for content type
        //mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // Add headers for content type
        //mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        //*******************DEBUG END************************
//        print("******** NEW API REQUEST ********")
//        print("Url: \(URL?.absoluteString)")
//        print("Method: \(method.rawValue)")
//        print("Body Parameters: \(JSON(bodyParameters ?? [:]))")
//        print("Query String Parameters: \(JSON(queryStringParameters ?? [:]))")
        //*******************DEBUG END************************
        // Make request
        //        let request = Alamofire.request(mutableURLRequest.copy() as! URLRequest)
        mutableURLRequest.responseString(encoding: String.Encoding.utf8) { (alamofireResponse) -> Void in
            //*******************DEBUG************************
//            print("******** API RESPONSE ********")
            let req = alamofireResponse.request
            let result = alamofireResponse.result
            let res = alamofireResponse.response
            let error = alamofireResponse.result.error
            //If we have no error
            if error == nil  {
                //If we have got JSON response.
                if  res != nil && self.isJSON(res!) {
//                    print("Status Code: \(res!.statusCode)")
//                    print("Headers: \(JSON(res!.allHeaderFields))")
                    let json = self.parseBody(result.value ?? "")
//                    print("Body: \(json)")
                } else {
//                    print("Body: \(result.value)")
                }
            } else {
//                print("Error: \(error!.localizedDescription)")
            }
            //*******************DEBUG END************************
            // Handle Response (support both JSON and Plain Text responses)
            self.handleResponse(req!, response: res, body: result.value, error: error as NSError?, callback: callback)
        }
    }

    private class func handleResponse(alamofireResponse: DataResponse<String>, callback: @escaping APIServiceCallback) {
        //*******************DEBUG************************
        print("******** API RESPONSE ********")
        let req = alamofireResponse.request
        let result = alamofireResponse.result
        let res = alamofireResponse.response
        let error = alamofireResponse.result.error
        //If we have no error
        if error == nil  {
            //If we have got JSON response.
            if  res != nil && self.isJSON(res!) {
                //                    print("Status Code: \(res!.statusCode)")
                //                    print("Headers: \(JSON(res!.allHeaderFields))")
                let json = self.parseBody(result.value ?? "")
//                print("Body: \(json)")
            } else {
//                print("Body: \(result.value)")
            }
        } else {
//            print("Error: \(error!.localizedDescription)")
        }
        //*******************DEBUG END************************
        // Handle Response (support both JSON and Plain Text responses)
        self.handleResponse(req!, response: res, body: result.value, error: error as NSError?, callback: callback)
        
    }

    private class func handleResponse(_ request: URLRequest, response: HTTPURLResponse?, body: String?, error: NSError?, callback: @escaping APIServiceCallback) {
        
        // Check for request errors OR reponse is empty
        if error != nil || response == nil  {
            callback(JSON.null, NSError(uwgError: .networkFailure, innerError: error))
            return
        }
        
        if response!.statusCode < 200 || response!.statusCode > 299 {
            // If error, attempt to get the message from Body
            callback(JSON.null, NSError(uwgError: .networkFailure, code: response!.statusCode))
            return
        }
        
        // 200-299 with JSON body
        if isJSON(response!) {
            callback(parseBody(body ?? ""), nil)
            return
        }
        
        // 200-299 with plain/text body, attempt converting body to JSON, if not, assume error
        if body != nil && body!.isEmpty == false {
            let json = parseBody(body!)
            if json == JSON.null {
                callback(JSON.null, NSError(uwgError: .unknownAPIResponseFormat))
                return
            }
            
            callback(json, nil)
            return
        }
        else {
            // 200-299, empty body
            callback(JSON.null, nil)
        }
    }
    
    private class func isJSON(_ response: HTTPURLResponse) -> Bool {
        if let contentType = response.allHeaderFields["Content-Type"] as? String {
            let retVar : Bool = contentType.range(of: "application/json") != nil
            return retVar;
        }
        return false
    }
    
    /// Parse plain text string into SwiftyJSON, return null if string is not JSON
    class func parseBody(_ body: String) -> JSON {
        if let dataFromString = body.data(using: String.Encoding.utf8, allowLossyConversion: false) {
            return JSON(data: dataFromString)
        }
        return JSON.null
    }
    
    private class func buildAPIURL(_ path: String) -> String {
        return APIService.baseURL + path
    }
}

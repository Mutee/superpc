//
//  VoidReport.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 3/2/18.
//  Copyright © 2018 Muhammad Asif. All rights reserved.
//

import Foundation
import SwiftyJSON

struct VoidReport {

    var amount: Double
    var employeeName: String
    var orderNo: String
    var reason: String
    var dateTime: String

    init(json: JSON) {
        amount = json["voidamount"].doubleValue
        employeeName = json["employeeid"].stringValue
        orderNo = json["orderid"].stringValue
        reason = json["voidreason"].stringValue
        dateTime = json["voiddatetime"].stringValue
    }
}

//
//  User.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 12/19/17.
//  Copyright © 2017 Muhammad Asif. All rights reserved.
//

import Foundation
import SwiftyJSON
class User {
   
    var email: String
    var password: String
    var restaurants: [Restaurant] = []
    var haveAccessToReports: Bool = false
    
    init(json: JSON) {
        email = json["email"].stringValue
        password = json["password"].stringValue
        restaurants = json["companies"].arrayValue.map{Restaurant(json: $0)}
        haveAccessToReports = (json["report_access"].intValue == 1) ? true : false
    }
    func serializeAsJSONDictionary() -> JSONDictonary {
        
        var jsonDict = JSONDictonary()
        jsonDict["companies"] = restaurants.map{$0.serializeAsJSONDictionary()} as AnyObject?
        jsonDict["email"] = email as AnyObject?
        jsonDict["password"] = password as AnyObject?
        jsonDict["report_access"] = haveAccessToReports as AnyObject?
        return jsonDict
    }

}

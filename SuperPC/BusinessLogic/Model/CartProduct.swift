//
//  CartProduct.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 1/8/18.
//  Copyright © 2018 Muhammad Asif. All rights reserved.
//

import Foundation

struct CartProduct {
  
    var product: Product
    var quantity: Int
    
    init(product: Product, quantity: Int) {
        self.product = product
        self.quantity = quantity
    }
    func serializeAsJSONDictionary() -> JSONDictonary {
        var jsonDict = JSONDictonary()
        jsonDict["title"] = product.rawValueForServer as AnyObject
        jsonDict["qty"] = "\(quantity)" as AnyObject
        
        return jsonDict
    }
}

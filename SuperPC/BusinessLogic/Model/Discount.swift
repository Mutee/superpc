//
//  Discount.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 3/2/18.
//  Copyright © 2018 Muhammad Asif. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Discount {
    var amount: Double
    var name: String
    var employeeName: String
    var orderNo: String
    var dateTime: String
    var type: DiscountType
    
    init(json: JSON, type: DiscountType) {
        
        self.type = type
        
        switch type {
        case .cash:
            amount = json["CashDiscountAmount"].doubleValue
            name = "Cash Discount"
            employeeName = json["CashDiscountApprovalEmpID"].stringValue
        default:
            amount = json["DiscountAmountUsed"].doubleValue
            name = json["DiscountID"].stringValue
            employeeName = json["EmployeeID"].stringValue

        }
        orderNo = json["OrderID"].stringValue
        dateTime = json["OrderDateTime"].stringValue
        
    }
}

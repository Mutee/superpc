//
//  Attachment.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 1/3/18.
//  Copyright © 2018 Muhammad Asif. All rights reserved.
//

import Foundation

struct Attachment {
    var image: UIImage
    var name: String
    
    init(name: String, image: UIImage) {
        self.name = name
        self.image = image
    }
}

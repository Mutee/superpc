//
//  Restaurant.swift
//  SuperPC
//
//  Created by Mutee ur Rehman on 12/19/17.
//  Copyright © 2017 Muhammad Asif. All rights reserved.
//

import Foundation
import SwiftyJSON

class Restaurant {
    var name: String
    var businessPhone: String
    var streamingId: String
    var isActive: Bool = false
    
    init(json: JSON) {
        name = json["name"].stringValue
        businessPhone = json["business_phone_1"].stringValue.digits
        streamingId = json["streaming_id"].stringValue
        if json["isActive"] != nil {
            isActive = json["isActive"].boolValue
        }

    }
    func serializeAsJSONDictionary() -> JSONDictonary {
        
        var jsonDict = JSONDictonary()
        
        jsonDict["name"] = name as AnyObject?
        jsonDict["business_phone_1"] = businessPhone as AnyObject?
        jsonDict["streaming_id"] = streamingId as AnyObject?
        jsonDict["isActive"] = isActive as AnyObject?
        
        return jsonDict
    }

}
